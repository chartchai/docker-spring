package gistdacamt.simpleserverwithprofile;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SimpleServerWithProfileApplication {

    public static void main(String[] args) {
        SpringApplication.run(SimpleServerWithProfileApplication.class, args);
    }

}

