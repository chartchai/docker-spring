package gistdacamt.simpleserverwithprofile.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@RestController
public class TestController {
    @Value("${showText}")
    String showText;

    @GetMapping("/test")
    ResponseEntity<?> getShowText() {
        return ResponseEntity.ok(showText);
    }

    @Value("${fileLocation}")
    String fileDir;

    @GetMapping("/fileList")
    ResponseEntity<?> getFileList() throws IOException {
        File[] files = new File(fileDir).listFiles();
        List<String> fileName = Stream.of(files).map(p -> p.getName())
                .collect(Collectors.toList());
        return ResponseEntity.ok(fileName);

    }
}
