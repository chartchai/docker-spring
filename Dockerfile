FROM openjdk:8-jdk-alpine

ADD target/testServer.jar app.jar
EXPOSE 8080

ENV JAVA_PROFILE dev
RUN mkdir /fileDir
VOLUME fileDir


ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom",\
    "-Dspring.profiles.active=${JAVA_PROFILE}",\
    "-jar","/app.jar"]